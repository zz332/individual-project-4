use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};

// throws a dice using random number generator
fn dice_throw(command: String) -> Result<String, Box<dyn std::error::Error>> {
    print!("{command}");
    let result = rand::thread_rng().gen_range(1..=6);
    Ok(result.to_string())
}

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let command = event.payload.command;
    let message = match dice_throw(command.to_string()) {
        Ok(throw_result) => {
            format!("{}", throw_result)
        }
        Err(err) => {
            format!("Error in dice throw: {:?}", err)
        }
    };
    let resp = Response {
        req_id: event.context.request_id,
        msg: message,
    };
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(function_handler)).await
}
