use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct DiceThrow {
    pub throw_id: String,
    pub throw_value: String,
}

#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

#[derive(Deserialize)]
struct Request {
    req_id: String,
    msg: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let req_id = event.payload.req_id;
    let msg = event.payload.msg;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let dice_throw = DiceThrow {
        throw_id: req_id.clone(),
        throw_value: msg,
    };

    let throw_id = AttributeValue::S(dice_throw.throw_id.clone());
    let throw_value = AttributeValue::S(dice_throw.throw_value.to_string());

    let _resp = client
        .put_item()
        .table_name("dicethrow")
        .item("throw_id", throw_id)
        .item("throw_value", throw_value)
        .send()
        .await
        .map_err(|_err| FailureResponse {
            body: _err.to_string(),
        });

    let resp = Response {
        req_id: event.context.request_id,
        msg: "Inserted into db".to_string(),
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(function_handler)).await
}
