rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 		
	cargo --version 			
	rustfmt --version			
	rustup --version			
	clippy-driver --version		

format:
	cargo fmt --quiet

install:
	# Install if needed
	#@echo "Updating rust toolchain"
	#rustup update stable
	#rustup default stable 

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

watch:
	cargo lambda watch

build1: 
	cargo lambda build --bin throw --release 

build2: 
	cargo lambda build --bin store --release 


deploy1:
	cargo lambda deploy throw

deploy2:
	cargo lambda deploy store


aws-invoke: 
	cargo lambda invoke --remote throw --data-ascii "{ \"command\": \"throw\"}" 

invoke:
	cargo lambda invoke --data-ascii "{ \"command\": \"throw\"}" 

all: format lint test 
