# Individual Project 4
The purpose of this project is to expirement with step functions. I do this by building 2 lambda functuons: one to throw a dice, and the other to store the number from 1-6 to a database with judgement.

## Demo Video 
[Watch the video](https://youtu.be/VmxdQjN3JDg)


## Screenshots show my statemachine running

![1](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG145.jpg)

![2](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG146.jpg)

![3](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG147.jpg)


## Preparation 
1. Run `cargo lambda new individualproject4`
2. Build rust functions in `src/main.rs` and `src/store.rs`
3. Add dependencies and bins in `Cargo.toml`
4. Run `cargo lambda watch` to test locally
5. Run `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test the Lambda function 
6. Go to the AWS IAM web management console and add an IAM User for credentials.
7. Attach policies `lambdafullaccess` and `iamfullaccess`
8. Generate access key.
9. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION  in a `.env` file 
10. Export the vars by using `export` in the terminal
11. Build and deploy porjects:
    ```txt
    cargo lambda build --bin throw --release
    cargo lambda build --bin store --release
    cargo lambda deploy throw
    cargo lambda deploy store
    ```
12. Attach policies `DynamoDBfullaccess` to each roles
13. Navigate to "State Machines" in AWS and create a new state machine
14. Drag lambda functions to the state machine
15. Test your state machine the result is the response of the final step function
